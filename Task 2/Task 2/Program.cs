﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter month you were born");
            var month = Console.ReadLine();

            Console.WriteLine("Please enter date you were born");
            var date = Console.ReadLine();

            Console.WriteLine($"You were born on {month} {date}");
        }

    }
}
