﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_4
{
    class Program
    {
        static void Main(string[] args)
        {
            
            double Celsius = 0;
            double Fahrenheit = 0;
            Console.WriteLine("Please enter celsius or fahrenheit to convert");
            var answer = Console.ReadLine();
           
            int caseSwitch = 1;
            switch (answer) 
            {
                case "celsius":
                    Console.WriteLine("Please enter degrees you want to convert");
                    Celsius = double.Parse(Console.ReadLine());
                    Console.WriteLine($"celsius converted into fahrenheit equals {(((Celsius * 9) / 5) + 32)} fahrenheit");                    
                    break;
                case "fahrenheit":
                    Console.WriteLine("Please enter degrees you want to convert");
                    Fahrenheit = double.Parse(Console.ReadLine());
                    Console.WriteLine($"fahrenheit converted into celsius equals {(((Fahrenheit -32) * 5 ) / 9 )} celsius");
                    break;
                default:
                    Console.WriteLine("the selection was invalid");
                    break;
            }
        }

    }
}
